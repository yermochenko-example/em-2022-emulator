package by.vsu.emulator.memory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MemoryUnitArrayImplTest {
	private MemoryUnitArrayImpl memoryUnit;

	@BeforeEach
	void setUp() {
		memoryUnit = new MemoryUnitArrayImpl(9, 8, 7, 6);
	}

	@Test
	void readCorrectAddressTest() throws IncorrectAddressException {
		assertEquals(7, memoryUnit.read(2));
	}

	@Test
	void readIncorrectAddressTest() {
		assertThrows(IncorrectAddressException.class, () -> memoryUnit.read(7));
	}

	@Test
	void writeCorrectAddressTest() throws IncorrectAddressException {
		memoryUnit.write(2, 5);
		assertArrayEquals(new int[] {9, 8, 5, 6}, memoryUnit.readRaw());
	}

	@Test
	void writeIncorrectAddressTest() {
		assertThrows(IncorrectAddressException.class, () -> memoryUnit.write(7, 10));
	}
}
