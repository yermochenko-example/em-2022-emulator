package by.vsu.emulator.cu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ControlUnitTest {
	private ControlUnitImpl controlUnitImpl;

	@BeforeEach
	void setUp() {
		controlUnitImpl = new ControlUnitImpl(5, 2, 3, 4);
	}

	@Test
	void popCorrectTest() throws StackEmptyException {
		assertEquals(4, controlUnitImpl.pop());
		assertEquals(2, controlUnitImpl.readStackPointerRaw());
	}

	@Test
	void popIncorrectTest() {
		assertThrows(StackEmptyException.class, () -> new ControlUnitImpl(5).pop());
	}

	@Test
	void pushCorrectTest() throws StackFullException {
		controlUnitImpl.push(5);
		assertArrayEquals(new int[] {2, 3, 4, 5, 0}, controlUnitImpl.readStackRegistersRaw());
		assertEquals(4, controlUnitImpl.readStackPointerRaw());
	}

	@Test
	void pushIncorrectTest() {
		assertThrows(StackFullException.class, () -> new ControlUnitImpl(3, 1, 1, 1).push(1));
	}
}
