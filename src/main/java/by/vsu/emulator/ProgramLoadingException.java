package by.vsu.emulator;

public class ProgramLoadingException extends Exception {
	public ProgramLoadingException(Throwable cause) {
		super(cause);
	}
}
