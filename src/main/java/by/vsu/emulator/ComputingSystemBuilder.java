package by.vsu.emulator;

import by.vsu.emulator.alu.*;
import by.vsu.emulator.cu.ControlUnit;
import by.vsu.emulator.cu.ControlUnitImpl;
import by.vsu.emulator.memory.MemoryUnit;
import by.vsu.emulator.memory.MemoryUnitArrayImpl;

import java.util.HashMap;
import java.util.Map;

public class ComputingSystemBuilder {
	public static ControlUnit build() {
		MemoryUnit memory = new MemoryUnitArrayImpl(256);
		ControlUnitImpl controlUnit = new ControlUnitImpl(16);
		Map<Integer, Operation> arithmeticLogicUnit = new HashMap<>();
		arithmeticLogicUnit.put(11, new PushNumberOperation());
		arithmeticLogicUnit.put(12, new PushVariableOperation());
		arithmeticLogicUnit.put(13, new PopVariableOperation());
		arithmeticLogicUnit.put(21, new AddOperation());
		arithmeticLogicUnit.put(22, new SubOperation());
		arithmeticLogicUnit.put(23, new MulOperation());
		arithmeticLogicUnit.put(24, new DivOperation());
		arithmeticLogicUnit.put(25, new ModOperation());
		arithmeticLogicUnit.put(31, new OutputStringOperation());
		arithmeticLogicUnit.put(32, new OutputNumberOperation());
		arithmeticLogicUnit.put(33, new InputNumberOperation());
		arithmeticLogicUnit.put(40, new ExitOperation());
		controlUnit.setMemoryUnit(memory);
		controlUnit.setOperations(arithmeticLogicUnit);
		for(Operation operation : arithmeticLogicUnit.values()) {
			operation.setMemory(memory);
			operation.setRegisters(controlUnit);
		}
		return controlUnit;
	}
}
