package by.vsu.emulator;

import java.util.Scanner;

public class StringToCodesConverterMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter string: ");
		String s = scanner.nextLine();
		System.out.printf("String \"%s\" is presented by following numeric codes:\n", s);
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			System.out.println((int) c);
		}
	}
}
