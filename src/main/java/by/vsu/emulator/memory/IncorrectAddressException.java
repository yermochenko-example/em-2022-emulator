package by.vsu.emulator.memory;

public class IncorrectAddressException extends Exception {
	private final int incorrectAddress;

	public IncorrectAddressException(int incorrectAddress) {
		this.incorrectAddress = incorrectAddress;
	}

	public int getIncorrectAddress() {
		return incorrectAddress;
	}

	@Override
	public String getMessage() {
		return String.format("Address %d is incorrect", incorrectAddress);
	}
}
