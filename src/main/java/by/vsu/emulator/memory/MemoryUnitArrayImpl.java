package by.vsu.emulator.memory;

public class MemoryUnitArrayImpl implements MemoryUnit {
	private final int[] cells;

	public MemoryUnitArrayImpl(int size) {
		this.cells = new int[size];
	}

	MemoryUnitArrayImpl(int... cells) {
		this.cells = cells;
	}

	@Override
	public int read(int address) throws IncorrectAddressException {
		checkAddressCorrect(address);
		return cells[address];
	}

	@Override
	public void write(int address, int value) throws IncorrectAddressException {
		checkAddressCorrect(address);
		cells[address] = value;
	}

	int[] readRaw() {
		return cells;
	}

	private void checkAddressCorrect(int address) throws IncorrectAddressException {
		if(!(0 <= address && address < cells.length)) {
			throw new IncorrectAddressException(address);
		}
	}
}
