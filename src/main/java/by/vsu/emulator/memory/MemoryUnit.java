package by.vsu.emulator.memory;

public interface MemoryUnit {
	int read(int address) throws IncorrectAddressException;

	void write(int address, int value) throws IncorrectAddressException;
}
