package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackEmptyException;

public class OutputNumberOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		try {
			int value = getRegisters().pop();
			System.out.print(value);
		} catch(StackEmptyException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 1;
	}
}
