package by.vsu.emulator.alu;

public class DivOperation extends ArithmeticOperation {
	@Override
	protected int calc(int fistOperand, int secondOperand) throws DivisionByZeroException {
		if(secondOperand != 0) {
			return fistOperand / secondOperand;
		} else {
			throw new DivisionByZeroException();
		}
	}
}
