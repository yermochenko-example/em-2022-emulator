package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackEmptyException;
import by.vsu.emulator.cu.StackFullException;

public abstract class ArithmeticOperation extends Operation {
	@Override
	public final void execute(int address) throws OperationException {
		try {
			int secondOperand = getRegisters().pop();
			int fistOperand = getRegisters().pop();
			int result = calc(fistOperand, secondOperand);
			getRegisters().push(result);
		} catch(StackEmptyException | StackFullException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 1;
	}

	protected abstract int calc(int fistOperand, int secondOperand) throws OperationException;
}
