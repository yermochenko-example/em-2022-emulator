package by.vsu.emulator.alu;

import by.vsu.emulator.cu.RegisterSet;
import by.vsu.emulator.memory.MemoryUnit;

public abstract class Operation {
	private RegisterSet registers;
	private MemoryUnit memory;

	public RegisterSet getRegisters() {
		return registers;
	}

	public void setRegisters(RegisterSet registers) {
		this.registers = registers;
	}

	public MemoryUnit getMemory() {
		return memory;
	}

	public void setMemory(MemoryUnit memory) {
		this.memory = memory;
	}

	public abstract void execute(int address) throws OperationException;

	public abstract int size();
}
