package by.vsu.emulator.alu;

public class MulOperation extends ArithmeticOperation {
	@Override
	protected int calc(int fistOperand, int secondOperand) {
		return fistOperand * secondOperand;
	}
}
