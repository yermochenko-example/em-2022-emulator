package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackEmptyException;
import by.vsu.emulator.memory.IncorrectAddressException;

public class PopVariableOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		try {
			int variableAddress = getMemory().read(address + 1);
			int variableValue = getRegisters().pop();
			getMemory().write(variableAddress, variableValue);
		} catch(IncorrectAddressException | StackEmptyException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 2;
	}
}
