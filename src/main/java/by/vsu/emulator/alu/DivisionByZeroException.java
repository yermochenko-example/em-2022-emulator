package by.vsu.emulator.alu;

public class DivisionByZeroException extends OperationException {
	public DivisionByZeroException() {
		super("Division by zero");
	}
}
