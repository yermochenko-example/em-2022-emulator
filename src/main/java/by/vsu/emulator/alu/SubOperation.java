package by.vsu.emulator.alu;

public class SubOperation extends ArithmeticOperation {
	@Override
	protected int calc(int fistOperand, int secondOperand) {
		return fistOperand - secondOperand;
	}
}
