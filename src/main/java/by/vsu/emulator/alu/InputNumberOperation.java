package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackFullException;

import java.util.Scanner;

public class InputNumberOperation extends Operation {
	private final static Scanner scanner = new Scanner(System.in);

	@Override
	public void execute(int address) throws OperationException {
		int value = scanner.nextInt();
		try {
			getRegisters().push(value);
		} catch(StackFullException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 1;
	}
}
