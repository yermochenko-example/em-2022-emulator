package by.vsu.emulator.alu;

public class ExitOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		this.getRegisters().setOperationPointer(0);
	}

	@Override
	public int size() {
		return 1;
	}
}
