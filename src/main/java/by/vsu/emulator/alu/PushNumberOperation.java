package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackFullException;
import by.vsu.emulator.memory.IncorrectAddressException;

public class PushNumberOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		try {
			int value = getMemory().read(address + 1);
			getRegisters().push(value);
		} catch(IncorrectAddressException | StackFullException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 2;
	}
}
