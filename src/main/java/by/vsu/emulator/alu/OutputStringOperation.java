package by.vsu.emulator.alu;

import by.vsu.emulator.memory.IncorrectAddressException;

public class OutputStringOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		try {
			int stringAddress = getMemory().read(address + 1);
			int characterCode;
			while((characterCode = getMemory().read(stringAddress)) != 0) {
				System.out.print((char)characterCode);
				stringAddress++;
			}
		} catch(IncorrectAddressException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 2;
	}
}
