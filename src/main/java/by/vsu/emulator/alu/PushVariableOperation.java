package by.vsu.emulator.alu;

import by.vsu.emulator.cu.StackFullException;
import by.vsu.emulator.memory.IncorrectAddressException;

public class PushVariableOperation extends Operation {
	@Override
	public void execute(int address) throws OperationException {
		try {
			int variableAddress = getMemory().read(address + 1);
			int variableValue = getMemory().read(variableAddress);
			getRegisters().push(variableValue);
		} catch(IncorrectAddressException | StackFullException e) {
			throw new OperationException(e);
		}
	}

	@Override
	public int size() {
		return 2;
	}
}
