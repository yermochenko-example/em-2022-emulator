package by.vsu.emulator.alu;

public class OperationException extends Exception {
	public OperationException(String message) {
		super(message);
	}

	public OperationException(Throwable cause) {
		super(cause);
	}
}
