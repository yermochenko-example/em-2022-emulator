package by.vsu.emulator;

import by.vsu.emulator.cu.ControlUnit;
import by.vsu.emulator.cu.ProgramExecutionException;

public class Main {
	public static void main(String[] args) {
		try {
			ControlUnit controlUnit = ComputingSystemBuilder.build();
			ProgramLoader.load(controlUnit, "program.txt");
			controlUnit.execute();
		} catch(ProgramLoadingException | ProgramExecutionException e) {
			e.printStackTrace();
		}
	}
}
