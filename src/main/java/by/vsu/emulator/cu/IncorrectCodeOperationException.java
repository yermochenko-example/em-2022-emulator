package by.vsu.emulator.cu;

public class IncorrectCodeOperationException extends ProgramExecutionException {
	private final int address;
	private final int code;
	public IncorrectCodeOperationException(int address, int code) {
		this.address = address;
		this.code = code;
	}

	@Override
	public String getMessage() {
		return String.format("Code of operation %d is incorrect. It was read at address %d", code, address);
	}

	public int getAddress() {
		return address;
	}

	public int getCode() {
		return code;
	}
}
