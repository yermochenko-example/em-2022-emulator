package by.vsu.emulator.cu;

import by.vsu.emulator.alu.Operation;
import by.vsu.emulator.alu.OperationException;
import by.vsu.emulator.memory.IncorrectAddressException;
import by.vsu.emulator.memory.MemoryUnit;

import java.util.List;
import java.util.Map;

public class ControlUnitImpl implements ControlUnit, RegisterSet {
	private MemoryUnit memoryUnit;
	private Map<Integer, Operation> operations;

	private final int[] stackRegisters;
	private int stackPointer;
	private int operationPointer;

	public ControlUnitImpl(int stackSize) {
		stackRegisters = new int[stackSize];
	}

	ControlUnitImpl(int size, int... stackRegisters) {
		this.stackRegisters = new int[size];
		stackPointer = stackRegisters.length;
		System.arraycopy(stackRegisters, 0, this.stackRegisters, 0, stackPointer);
	}

	public MemoryUnit getMemoryUnit() {
		return memoryUnit;
	}

	public void setMemoryUnit(MemoryUnit memoryUnit) {
		this.memoryUnit = memoryUnit;
	}

	public Map<Integer, Operation> getOperations() {
		return operations;
	}

	public void setOperations(Map<Integer, Operation> operations) {
		this.operations = operations;
	}

	@Override
	public int pop() throws StackEmptyException {
		if(stackPointer > 0) {
			stackPointer--;
			return stackRegisters[stackPointer];
		} else {
			throw new StackEmptyException();
		}
	}

	@Override
	public void push(int value) throws StackFullException {
		if(stackPointer < stackRegisters.length) {
			stackRegisters[stackPointer] = value;
			stackPointer++;
		} else {
			throw new StackFullException();
		}
	}

	@Override
	public int getOperationPointer() {
		return operationPointer;
	}

	@Override
	public void setOperationPointer(int value) {
		this.operationPointer = value;
	}

	public void execute() throws ProgramExecutionException {
		try {
			operationPointer = getMemoryUnit().read(0);
			while(operationPointer != 0) {
				int currentAddress = operationPointer;
				int operationCode = getMemoryUnit().read(currentAddress);
				Operation operation = operations.get(operationCode);
				if(operation != null) {
					operationPointer += operation.size();
					operation.execute(currentAddress);
				} else {
					throw new IncorrectCodeOperationException(currentAddress, operationCode);
				}
			}
		} catch(IncorrectAddressException | OperationException e) {
			throw new ProgramExecutionException(e);
		}
	}

	@Override
	public void loadProgram(List<Integer> program) throws IncorrectAddressException {
		for(int address = 0; address < program.size(); address++) {
			memoryUnit.write(address, program.get(address));
		}
	}

	int[] readStackRegistersRaw() {
		return stackRegisters;
	}

	int readStackPointerRaw() {
		return stackPointer;
	}
}
