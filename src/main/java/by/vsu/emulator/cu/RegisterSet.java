package by.vsu.emulator.cu;

public interface RegisterSet {
	int pop() throws StackEmptyException;
	void push(int value) throws StackFullException;
	int getOperationPointer();
	void setOperationPointer(int value);
}
