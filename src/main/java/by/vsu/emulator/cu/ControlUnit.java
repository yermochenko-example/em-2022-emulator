package by.vsu.emulator.cu;

import by.vsu.emulator.memory.IncorrectAddressException;

import java.util.List;

public interface ControlUnit {
	void loadProgram(List<Integer> program) throws IncorrectAddressException;
	void execute() throws ProgramExecutionException;
}
