package by.vsu.emulator.cu;

public class ProgramExecutionException extends Exception {
	public ProgramExecutionException() {}

	public ProgramExecutionException(Throwable cause) {
		super(cause);
	}
}
