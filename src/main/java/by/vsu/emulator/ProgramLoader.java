package by.vsu.emulator;

import by.vsu.emulator.cu.ControlUnit;
import by.vsu.emulator.memory.IncorrectAddressException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProgramLoader {
	public static void load(ControlUnit controlUnit, String fileName) throws ProgramLoadingException {
		try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String line;
			List<Integer> program = new ArrayList<>();
			while((line = reader.readLine()) != null) {
				Integer number = Integer.valueOf(line);
				program.add(number);
			}
			controlUnit.loadProgram(program);
		} catch(IOException | NumberFormatException | IncorrectAddressException e) {
			throw new ProgramLoadingException(e);
		}
	}
}
